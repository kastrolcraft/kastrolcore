package sk.perri.kc.kastrolcore.events;


import com.google.common.base.Strings;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import sk.perri.kc.kastrolcore.Main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChatListener implements Listener, CommandExecutor
{
    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event)
    {
        List<String> pmCommands = Arrays.asList("/pm", "/msg", "/w", "/tell");
        String[] s = event.getMessage().split(" ");

        for(String cmd : pmCommands) // Budem breakovat, nepouzivam lambdu
        {
            if(s[0].equalsIgnoreCase(cmd) && s.length > 1)
            {
                Player p = Bukkit.getPlayer(s[1]);
                if(p != null && Main.hraci.containsKey(p.getName()) && Main.hraci.get(p.getName()).isBusy())
                {
                    event.getPlayer().sendMessage(
                        ChatColor.translateAlternateColorCodes('&', Main.self.getConfig().getString("msg.busy-msg")));
                    event.setCancelled(true);
                    break;
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onChatMessage(AsyncPlayerChatEvent event)
    {

        // Check multiple chars
        String msg = event.getMessage();
        StringBuilder sb = new StringBuilder();

        char last = '0';
        int count = 0;
        for(char s : msg.toCharArray())
        {
            if(s == last)
                count++;
            else
            {
                last = s;
                count = 1;
            }

            if(count >= Main.self.getConfig().getInt("char-trash-holder"))
                continue;

            sb.append(s);
        }
        msg = sb.toString();

        // Check blacklist
        StringBuilder res = new StringBuilder();

        for(String slovo : msg.split(" "))
        {
            res.append(res.length() > 0 ? " " : "");
            if(Main.blacklist.contains(ChatColor.stripColor(slovo)))
                res.append(Strings.repeat(Main.self.getConfig().getString("block-char"), slovo.length()));
            else
                res.append(slovo);
        }

        // Check busy
        boolean cancel[] = {false};

        for(String slovo : msg.split(" "))
        {
            Main.hraci.forEach((n, h) ->
            {
                if(slovo.toLowerCase().contains(n.toLowerCase()) && h.isBusy())
                {
                    cancel[0] = true;
                }
            });
        }

        if(cancel[0] && !event.getPlayer().hasPermission("kastrolcore.overbusy"))
        {
            event.getPlayer().sendMessage(
                ChatColor.translateAlternateColorCodes('&', Main.self.getConfig().getString("msg.busy-msg")));
            event.setCancelled(true);
        }

        event.setMessage(res.toString());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(cmd.getName().equalsIgnoreCase("busy"))
        {
            if(sender.hasPermission("kastrolcore.busy"))
            {
                if(Main.hraci.containsKey(sender.getName()))
                {
                    if(Main.hraci.get(sender.getName()).isBusy())
                    {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                            Main.self.getConfig().getString("msg.busy-off")));
                        Main.hraci.get(sender.getName()).setBusy(false);
                    }
                    else
                    {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                            Main.self.getConfig().getString("msg.busy-on")));
                        Main.hraci.get(sender.getName()).setBusy(true);
                    }
                }
                else
                {
                    sender.sendMessage("§cBohuzial nastala chyba a neviem nastavit busy mod!");
                }
            }
        }

        return true;
    }
}
