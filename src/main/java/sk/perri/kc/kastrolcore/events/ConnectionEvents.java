package sk.perri.kc.kastrolcore.events;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import sk.perri.kc.kastrolcore.Main;
import sk.perri.kc.kastrolcore.classes.Hrac;

public class ConnectionEvents implements Listener
{
    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {
        Main.self.db.getPlayerData(event.getPlayer().getName());
        Bukkit.getServer().getScheduler().runTaskLater(Main.self, () ->
        {
            Hrac h = new Hrac(event.getPlayer(), Main.self.db.getPlayerData(event.getPlayer().getName()));
            Main.hraci.put(event.getPlayer().getName(), h);
        }, 10L);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event)
    {
        if(Main.hraci.containsKey(event.getPlayer().getName()))
            Main.hraci.remove(event.getPlayer().getName());
    }
}
