package sk.perri.kc.kastrolcore.events;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import sk.perri.kc.kastrolcore.Main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class VoteListener implements Listener, CommandExecutor
{
    @EventHandler
    public void onVote(VotifierEvent event)
    {
        Vote v = event.getVote();
        if(Main.hraci.containsKey(v.getUsername()))
            Main.hraci.get(v.getUsername()).setVoted(true);
        else if(!v.getUsername().equalsIgnoreCase(""))
            Main.self.db.offlineVote(v.getUsername());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(cmd.getName().equalsIgnoreCase("vote"))
        {
            boolean hlasoval = false;
            int pocetHlasov = -1;
            String session = "";
            Calendar now = Calendar.getInstance();
            int h = now.get(Calendar.HOUR_OF_DAY);
            session = ChatColor.translateAlternateColorCodes('&', String.format(
                Main.self.getConfig().getString("msg.vote.session"), Math.max(0, Math.round(Math.floor(h/2.0))*2),
                 Math.round(Math.ceil((h+1)/2.0)*2)%24));
            
            if(Main.hraci.containsKey(sender.getName()))
            {
                hlasoval = Main.hraci.get(sender.getName()).voted();
                pocetHlasov = Main.hraci.get(sender.getName()).getVotes();
            }
            
            TextComponent tc;

            if(!hlasoval)
            {
                tc = new TextComponent(ChatColor.translateAlternateColorCodes('&', Main.self.getConfig().getString(
                    "msg.vote.address")));
                tc.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL,
                    Main.self.getConfig().getString("voteaddress")+sender.getName()));
                tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(
                    ChatColor.translateAlternateColorCodes('&',
                        Main.self.getConfig().getString("msg.vote.hover"))).create()));
            }
            else
            {
                tc = new TextComponent(ChatColor.translateAlternateColorCodes('&',
                    Main.self.getConfig().getString("msg.vote.already-voted")));
            }


            int finalPocetHlasov = pocetHlasov;
            String finalSession = session;
            Main.self.getConfig().getStringList("msg.vote.vote").forEach(l ->
            {
                String res = ChatColor.translateAlternateColorCodes('&',
                    l.replace("%pocet_hlasov%", Integer.toString(finalPocetHlasov)).replace("%session%", finalSession));

                if(res.contains("%sprava%"))
                {
                    String[] r = res.split("%sprava%");
                    TextComponent t = new TextComponent(r[0]);
                    t.addExtra(tc);
                    if(r.length > 1)
                        t.addExtra(r[1]);

                    if(sender instanceof Player)
                        ((Player) sender).spigot().sendMessage(t);

                }
                else
                {
                    sender.sendMessage(res);
                }
            });
        }
        else if(cmd.getName().equalsIgnoreCase("votoval"))
        {
            if(args.length != 1)
            {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                    Main.self.getConfig().getString("msg.vote.votoval-usage")));
            }
            else
            {
                if(Main.hraci.containsKey(args[0]) && Main.hraci.get(args[0]).voted())
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                        Main.self.getConfig().getString("msg.vote.votoval-yes")));
                else
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                        Main.self.getConfig().getString("msg.vote.votoval-no")));
            }
        }
        else if(cmd.getName().equalsIgnoreCase("novotefinder"))
        {
            List<String> nv = new ArrayList<>();
            Main.self.getServer().getOnlinePlayers().forEach(p ->
            {
                if(!Main.hraci.containsKey(p.getName()) || !Main.hraci.get(p.getName()).voted())
                    nv.add(p.getName());
            });

            sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                Main.self.getConfig().getString("msg.vote.novote")+ StringUtils.join(nv.toArray(), ", ")));
        }

        return true;
    }
}
