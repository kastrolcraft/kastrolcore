package sk.perri.kc.kastrolcore.database;

import org.bukkit.Bukkit;
import sk.perri.kc.kastrolcore.Main;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class Database
{
    private Connection conn;

    public boolean connect()
    {
        try
        {
            conn = null;
            DriverManager.setLoginTimeout(1000);
            conn = DriverManager.getConnection("jdbc:mysql://" + Main.self.getConfig().getString("db.host") +
                    ":3306/" +Main.self.getConfig().getString("db.db") + "?useSSL=no&user="+
                    Main.self.getConfig().getString("db.user")+"&password=" +
                    Main.self.getConfig().getString("db.pass") + "&useUnicode=true&characterEncoding=UTF-8" +
                "&autoReconnect=true&failOverReadOnly=false&maxReconnects=10&connectTimeout=2000&socketTimeout=2000");
            return conn != null;
        }
        catch (Exception e)
        {
            Main.self.getLogger().warning("[KastrolCore] [E] Neviem sa pripojit ku databaze: "+e.toString());
            return false;
        }
    }

    public Map<String, Object> getPlayerData(String player)
    {
        Map<String, Object> res = new HashMap<>();

        try
        {
            String sqlV = "SELECT VotingPlugin_Users.Points as votes FROM VotingPlugin_Users WHERE " +
                "VotingPlugin_Users.PlayerName = '"+player+"'";

            String sqlS = "SELECT COUNT(*) as spawners FROM sl_records WHERE " +
                "sl_records.nick = '"+player+"'";

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sqlV);
            if(rs.next())
                res.put("votes", rs.getInt(1));
            else
                res.put("votes", -1);

            rs = st.executeQuery(sqlS);
            if(rs.next())
                res.put("spawners", rs.getInt(1));
            else
                res.put("spawners", 0);

            rs.close();
            return res;
        }
        catch (SQLException e)
        {
            Bukkit.getLogger().warning("[KastrolCore][E] SQLException pri vyberani hraca: "+player);
        }
        catch (Exception e)
        {
            Bukkit.getLogger().warning("[KastrolCore][E] Error pri vyberani hraca: "+player+" e: "+e.toString());
        }

        return null;
    }

    public void pingDatabase()
    {
        try
        {
            if(!conn.createStatement().executeQuery("SELECT * FROM VotingPlugin_Users LIMIT 1").next())
                connect();
        }
        catch(Exception e)
        {
            Bukkit.getLogger().warning("[KastrolCore][W] Error pri pingovani DB, vytvaram spojenie");
            connect();
        }
    }

    public void offlineVote(String player)
    {
        try
        {
            // TODO Opravit hovno-kod
            conn.createStatement().executeQuery("SELECT * FROM VotingPlugin_Users LIMIT 1");

            Bukkit.getServer().getScheduler().runTaskLater(Main.self, () ->
            {
                try
                {
                    if (!conn.createStatement().execute(
                        "UPDATE VotingPlugin_Users SET Points = Points + 1 WHERE PlayerName LIKE '" + player + "' LIMIT 1"))
                        Bukkit.getLogger().warning("[KastrolCore][W] Neviem zapisat offline vote hraca " + player);
                }
                catch (SQLException e)
                {
                    Bukkit.getLogger().warning("[KastrolCore][E] SQLException pri zapisovani offlineVote hraca: "+player+
                        " e: "+e.getMessage());
                }
                catch (Exception e)
                {
                    Bukkit.getLogger().warning("[KastrolCore][E] Error pri zapisovani offlineVote hraca: "+player+
                        " e: "+e.toString());
                }
            },10L);
        }
        catch (SQLException e)
        {
            Bukkit.getLogger().warning("[KastrolCore][E] SQLException pri zapisovani offlineVote hraca: "+player+
                " e: "+e.getMessage());
        }
        catch (Exception e)
        {
            Bukkit.getLogger().warning("[KastrolCore][E] Error pri zapisovani offlineVote hraca: "+player+
                " e: "+e.toString());
        }
    }
}
