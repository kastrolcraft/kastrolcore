package sk.perri.kc.kastrolcore.classes;

import javafx.util.Pair;
import org.bukkit.entity.Player;
import sk.perri.kc.kastrolcore.Main;

import java.util.Map;

public class Hrac
{
    private String nick;
    private Player player;
    private int spawners = 0;
    private int votes = -1;
    private boolean voted = false;
    private Pair<String, Integer> spawnerGroup = new Pair<>("", 0);
    private boolean busy = false;

    public Hrac(Player player, Map<String, Object> data)
    {
        this.player = player;
        nick = player.getName();
        if(data != null)
        {
            spawners = (int) data.getOrDefault("spawners", 0);
            votes = (int) data.getOrDefault("votes", -1);
        }

        // spawner group
        Main.spawnerGroups.forEach((g,l) ->
        {
            if(player.hasPermission("kastrolcore.spawner.group."+g) && l > spawnerGroup.getValue())
                spawnerGroup = new Pair<>(g, l);
        });
    }

    public void setVoted(boolean voted)
    {
        this.voted = voted;
        if(voted)
            votes++;
    }

    public boolean voted()
    {
        return voted;
    }

    public int getVotes()
    {
        return votes;
    }

    public boolean isBusy()
    {
        return busy;
    }

    public void setBusy(boolean busy)
    {
        this.busy = busy;
    }
}
