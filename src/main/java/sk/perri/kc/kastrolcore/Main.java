package sk.perri.kc.kastrolcore;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
import sk.perri.kc.kastrolcore.classes.Hrac;
import sk.perri.kc.kastrolcore.database.Database;
import sk.perri.kc.kastrolcore.events.ChatListener;
import sk.perri.kc.kastrolcore.events.ConnectionEvents;
import sk.perri.kc.kastrolcore.events.VoteListener;

import java.util.*;

public class Main extends JavaPlugin
{
    public static Main self;

    // Data holders
    public static Map<String, Hrac> hraci = new HashMap<>();
    public static Map<String, Integer> spawnerGroups = new HashMap<>();
    public static List<String> blacklist = new ArrayList<>();
    public Database db;

    public void onEnable()
    {
        self = this;

        if(!getDataFolder().exists())
            getDataFolder().mkdir();

        getConfig().options().copyDefaults(true);
        saveConfig();

        blacklist = getConfig().getStringList("blacklist");

        db = new Database();

        getServer().getScheduler().runTaskLater(this, () ->
        {
            if(db.connect())
            {
                getLogger().info("[I] Pripojene ku databaze!");
                getServer().getOnlinePlayers().forEach(p ->
                {
                    if(!hraci.containsKey(p.getName()))
                    {
                        Main.self.db.getPlayerData(p.getName());
                        Bukkit.getServer().getScheduler().runTaskLater(Main.self, () ->
                        {
                            Hrac h = new Hrac(p, Main.self.db.getPlayerData(p.getName()));
                            Main.hraci.put(p.getName(), h);
                        }, 10L);
                    }
                });
            }
            else
                getServer().getPluginManager().disablePlugin(this);
        }, 40L);

        getServer().getPluginManager().registerEvents(new ConnectionEvents(), this);

        getLogger().info("[I] Plugin sa zapol!");

        // BUSY + Chat listener
        ChatListener chl = new ChatListener();
        getCommand("busy").setExecutor(chl);
        getServer().getPluginManager().registerEvents(chl, this);


        // VOTE
        VoteListener vl = new VoteListener();
        getServer().getPluginManager().registerEvents(vl, this);
        getCommand("vote").setExecutor(vl);
        getCommand("votoval").setExecutor(vl);
        getCommand("novotefinder").setExecutor(vl);

        final boolean[] sent = {false, false};
        getServer().getScheduler().runTaskTimerAsynchronously(this, () ->
        {
            Calendar now = Calendar.getInstance();

            // PING DB
            if(now.get(Calendar.MINUTE) % 15 == 0 && !sent[1])
            {
                db.pingDatabase();
                sent[1] = true;
            }
            else if(now.get(Calendar.MINUTE) % 15 != 0)
                sent[1] = false;

            // CLEAR VOTES AND SEND NOTIFICATION
            if((now.get(Calendar.HOUR_OF_DAY) % 2) == 0)
            {
                if(!sent[0] && now.get(Calendar.MINUTE) == 0)
                {
                    getServer().getOnlinePlayers().forEach(p ->
                    {
                        if(hraci.containsKey(p.getName()))
                        {
                            hraci.get(p.getName()).setVoted(false);
                        }

                        p.sendTitle(ChatColor.translateAlternateColorCodes('&', getConfig().getString("msg.vote.title")),
                            ChatColor.translateAlternateColorCodes('&', getConfig().getString("msg.vote.subtitle")), 10, 70, 20);
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("msg.vote.refresh")));
                        p.performCommand("vote");
                    });

                    sent[0] = true;
                }
                else if(now.get(Calendar.MINUTE) == 0)
                    sent[0] = false;
            }
        }, 100,getConfig().getInt("votetimecheck")*20);
    }

    public void onDisable()
    {
        getLogger().info("[I] Plugin sa vypol!");
    }
}
